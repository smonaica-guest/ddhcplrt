_default: build

binary: build

binary-arch: build

binary-indep: build

build:
	@:

build-arch: build

build-indep: build

check:
	@:

clean:
	@:

distclean: clean

install: build
	install --preserve-timestamps --mode=755 -D	bin/ddhcplrt.py	$(DESTDIR)/usr/bin/ddhcplrt
	install --preserve-timestamps --mode=644 -D	man1/ddhcplrt.1	$(DESTDIR)/usr/share/man/man1/ddhcplrt.1

test:
	@:

