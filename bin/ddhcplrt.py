#!/usr/bin/env python3
import datetime, bisect, re, sys, os

def parse_timestamp(raw_str):
	tokens = raw_str.split()
	
	if len(tokens) == 1:
		if tokens[0].lower() == 'never':
			return 'never';
		else:
			raise Exception('Parse error in timestamp')
	
	elif len(tokens) == 3:
		return datetime.datetime.strptime(' '.join(tokens[1:]),
			'%Y/%m/%d %H:%M:%S')
		
	else:
		raise Exception('Parse error in timestamp')


def timestamp_is_ge(t1, t2):
	if t1 == 'never':
		return True
	
	elif t2 == 'never':
		return False
	
	elif isinstance(t1, datetime.datetime) and isinstance(t2, datetime.datetime):
		return t1 >= t2
	
	else:
		return False


def timestamp_is_lt(t1, t2):
	if t1 == 'never':
		return False
		
	elif t2 == 'never':
		return t1 != 'never'
		
	elif isinstance(t1, datetime.datetime) and isinstance(t2, datetime.datetime):
		return t1 < t2
	
	else:
		return False


def timestamp_is_between(t, tstart, tend):
	return timestamp_is_ge(t, tstart) and timestamp_is_lt(t, tend)


def parse_hardware(raw_str):
	tokens = raw_str.split()
	
	if len(tokens) == 2:
		return tokens[1]
	
	else:
		raise Exception('Parse error in hardware')


def strip_endquotes(raw_str):
	return raw_str.strip('"')


def identity(raw_str):
	return raw_str


def parse_binding_state(raw_str):
	tokens = raw_str.split()
	
	if len(tokens) == 2:
		return tokens[1]
	
	else:
		raise Exception('Parse error in binding state')


def parse_next_binding_state(raw_str):
	tokens = raw_str.split()
	
	if len(tokens) == 3:
		return tokens[2]
	
	else:
		raise Exception('Parse error in next binding state')


def parse_rewind_binding_state(raw_str):
	tokens = raw_str.split()
	
	if len(tokens) == 3:
		return tokens[2]
	
	else:
		raise Exception('Parse error in rewind binding state')


def parse_uid(raw_str):
	return strip_endquotes(raw_str).replace('|', r'\174')
	

def parse_leases_file(leases_file):
	valid_keys = {
		'starts':		{ 'parser': parse_timestamp,		'default': 'unknown' },
		'ends':			{ 'parser': parse_timestamp,		'default': 'unknown' },
		'tstp':			{ 'parser': parse_timestamp,		'default': 'unknown' },
		'tsfp':			{ 'parser': parse_timestamp,		'default': 'unknown' },
		'atsfp':		{ 'parser': parse_timestamp,		'default': 'unknown' },
		'cltt':			{ 'parser': parse_timestamp,		'default': 'unknown' },
		'hardware':		{ 'parser': parse_hardware,		'default': '00:00:00:00:00:00' },
		'binding':		{ 'parser': parse_binding_state,	'default': '' },
		'next':			{ 'parser': parse_next_binding_state,	'default': '' },
		'rewind':		{ 'parser': parse_rewind_binding_state,	'default': '' },
		'uid':			{ 'parser': parse_uid,			'default': '' },
		'client-hostname':	{ 'parser': strip_endquotes,		'default': '' },
		'option':		{ 'parser': identity,			'default': '' },
		'set':			{ 'parser': identity,			'default': '' },
		'on':			{ 'parser': identity,			'default': '' },
		'abandoned':		{ 'parser': identity,			'default': '' },
		'bootp':		{ 'parser': identity,			'default': '' },
		'dynamic-bootp':	{ 'parser': identity,			'default': '' },
		'reserved':		{ 'parser': identity,			'default': '' },
		}
	
	leases_by_ip = {}
	leases_by_mac = {}
	
	lease_rec = {}
	in_lease = False
	in_failover = False
	
	for line in leases_file:
		if line.lstrip().startswith('#'):
			continue
		
		tokens = line.split()
		
		if len(tokens) == 0:
			continue
		
		key = tokens[0].lower().strip().rstrip(';').rstrip()
		
		if key == 'lease':
			if in_lease or in_failover:
				raise Exception('Parse error in leases file')
			else:
				ip_address = tokens[1]
				
				lease_rec = {'ip_address' : ip_address}
				in_lease = True
				
		elif key == 'failover':
			if in_lease or in_failover:
				raise Exception('Parse error in leases file')
			else:
				in_failover = True
				
		elif key == '}':
			if in_lease:
				for k in valid_keys:
					lease_rec[k] = lease_rec.get(k, valid_keys[k]['default'])
					
				ip_address = lease_rec['ip_address']
				ip_addr_int = ipv4_to_int(ip_address)
				mac = lease_rec.get('hardware', valid_keys['hardware']['default'])
				
				if ip_addr_int in leases_by_ip:
					if leases_by_ip[ip_addr_int][0]['hardware'] == mac:
						leases_by_ip[ip_addr_int] = leases_by_ip[ip_addr_int][1:]
					
					leases_by_ip[ip_addr_int].insert(0, lease_rec)
				else:
					leases_by_ip[ip_addr_int] = [lease_rec]
				
				if mac in leases_by_mac:
					if leases_by_mac[mac][0]['ip_address'] == ip_address:
						leases_by_mac[mac] = leases_by_mac[mac][1:]
					
					leases_by_mac[mac].insert(0, lease_rec)
				else:
					leases_by_mac[mac] = [lease_rec]
				
				lease_rec = {}
				in_lease = False
				
			elif in_failover:
				in_failover = False
				
			else:
				raise Exception('Parse error in leases file')
			
		elif key in valid_keys:
			if in_lease:
				value = line[(line.index(tokens[0]) + len(tokens[0])):]
				value = value.strip().rstrip(';').rstrip()
				
				lease_rec[key] = valid_keys[key]['parser'](value)
				
			elif in_failover:
				pass
				
			else:
				raise Exception('Parse error in leases file')
			
		else:
			if in_lease:
				raise Exception('Parse error in leases file')
				
	if in_lease or in_failover:
		raise Exception('Parse error in leases file')
	
	return {'by_ip': leases_by_ip, 'by_mac': leases_by_mac}


def round_datetime(dtime):
	if dtime.microsecond < 500000:
		delta_usecs = - dtime.microsecond
	else:
		delta_usecs = 1000000 - dtime.microsecond
	
	return dtime + datetime.timedelta(microseconds=delta_usecs)


def timestamp_now():
	return round_datetime(datetime.datetime.utcnow())


def lease_is_active(lease_rec, as_of_ts):
	return (lease_rec['binding'] == 'active') or \
		((lease_rec['binding'] != 'abandoned') and \
		timestamp_is_between(as_of_ts, lease_rec['starts'], lease_rec['ends']))


def ipv4_to_int(ipv4_addr):
	parts = ipv4_addr.split('.')
	return (int(parts[0]) << 24) + (int(parts[1]) << 16) + \
		(int(parts[2]) << 8) + int(parts[3])


def find_oui_file_path(script_dir):
	test_paths = [
		'/var/lib/ieee-data-sd57/oui.txt',
		'/var/lib/ieee-data/oui.txt',
		'/usr/share/ieee-data/oui.txt',
		os.path.join(script_dir, 'oui.txt'),
		'/usr/local/share/ddhcplrt/oui.txt',
		'/usr/share/ddhcplrt/oui.txt',
		]

	for oui_path in test_paths:
		if os.path.exists(oui_path):
			return oui_path

	return ''


def load_oui_lookup_database(oui_file):
	oui_db = {}
	
	for line in oui_file:
		p, h, c = [s.strip() for s in line.partition('(hex)')]
		
		if h == '(hex)':
			if re.match(r'^[0-9A-F]{2}(-[0-9A-F]{2}){2}$', p):
				oui_db[p] = c
	
	return oui_db


def mac_type(first_octet):
	if first_octet & 0x1 == 0x0:
		return 'Unicast'
	else:
		return 'Multicast'


def oui_lookup(oui_db, mac):
	prefix = mac[:8].upper().replace(':', '-')
	first_octet = int(prefix[0:2], 16)
	
	if first_octet & 0x2 == 0x2:
		return 'Local_Admin_' + mac_type(first_octet)
	else:
		return oui_db.get(prefix, 'unknown')


def select_active_leases(leases_db, as_of_ts):
	retarray = []
	sortedarray = []
	
	for ip_as_int in leases_db:
		for lease_rec in [ leases_db[ip_as_int][0] ]:
			if lease_is_active(lease_rec, as_of_ts):
				insertpos = bisect.bisect(sortedarray, ip_as_int)
				sortedarray.insert(insertpos, ip_as_int)
				retarray.insert(insertpos, lease_rec)
	
	return retarray


def augment_lease_record(lease_rec, now, oui_db):
	lease_rec['hardware-company'] = oui_lookup(oui_db, lease_rec.get('hardware', '00-00-00'))
	lease_rec['expires'] = str(lease_rec['ends'] - now) if lease_rec['ends'] not in ('unknown', 'never') else 'never'
	lease_rec['expired'] = str(now - lease_rec['ends']) if lease_rec['ends'] not in ('unknown', 'never') else 'never'
	lease_rec['last-contact'] = str(now - lease_rec['cltt']) if lease_rec['cltt'] not in ('unknown', 'never') else 'unknown'
	lease_rec['is-active'] = 'yes' if lease_is_active(lease_rec, now) else 'no'
	lease_rec['duration'] = str(lease_rec['ends'] - lease_rec['starts']) if (lease_rec['ends'] not in ('unknown', 'never') and lease_rec['starts'] not in ('unknown', 'never')) else 'infinite'
	lease_rec['began'] = str(now - lease_rec['starts']) if lease_rec['starts'] not in ('unknown', 'never') else 'unknown'
	lease_rec['client-string'] = lease_rec['client-hostname']
	
	if lease_rec['client-string'] == '':
		if lease_rec['uid'] != '':
			lease_rec['client-string'] = 'UID:' + lease_rec['uid']


def augment_active_leases_list(leases_list, now, oui_db):
	ref_num = -1
	mac_refs = {}
	
	for lease in leases_list:
		augment_lease_record(lease, now, oui_db)
		
		ref_num = ref_num + 1
		lease['ref'] = ref_num
		
		mac = lease['hardware']
		
		if mac in mac_refs:
			mac_refs[mac].append(ref_num)
		else:
			mac_refs[mac] = [ref_num]
		
	for mac in mac_refs:
		ref_list = mac_refs[mac]
		mac_refs[mac] = ref_list[1:] + ref_list[0:1]
	
	for lease in leases_list:
		mac = lease['hardware']
		mac_ref = mac_refs[mac][0]
		mac_refs[mac] = mac_refs[mac][1:]
		
		if mac_ref != lease['ref']:
			lease['also-has-ip'] = leases_list[mac_ref]['ip_address']
		else:
			lease['also-has-ip'] = ''
	
	return leases_list


def search_for_mac(line):
	return re.search(r'[0-9A-F]{2}([:-][0-9A-F]{2}){5}', line, re.IGNORECASE)


def get_mac(match):
	return match.group(0).lower().replace('-', ':')


def get_mac2(match):
	return match.group(0).lower().replace(':', '-')


def print_usage_and_exit(script_path):
	print('Usage: ' + os.path.basename(script_path) + ' report-active')
	print('   or: ' + os.path.basename(script_path) + ' report-full [ip|mac]')
	print('   or: ' + os.path.basename(script_path) + ' filter-by-mac [out|in] <file1> [<file2> ...]')
	print('   or: ' + os.path.basename(script_path) + ' dump-macs [normal|shorewall]')
	print()
	print('   Hint: Command names can be abbreviated by their initials')
	print('      e.g.: report-active ==> ra')
	print()
	sys.exit(1)


def report_common_setup(script_path):
	script_dir = os.path.dirname(script_path)
	
	oui_path = find_oui_file_path(script_dir)

	if oui_path != '':
		oui_file = open(oui_path, 'r')
		oui_db = load_oui_lookup_database(oui_file)
		oui_file.close()
	else:
		oui_db = {}
	
	leases_path = os.path.join(script_dir, 'dhcpd.leases')
	leases_live = False
	
	if not os.path.exists(leases_path):
		leases_path = os.path.join(os.path.sep, 'var', 'lib', 'dhcp', 'dhcpd.leases')
		leases_live = True
	
	leases_file = open(leases_path, 'r')
	raw_leases = parse_leases_file(leases_file)
	leases_file.close()
	
	leases_file_mtime = round_datetime(datetime.datetime.utcfromtimestamp(os.path.getmtime(leases_path)))
	
	actual_now = timestamp_now()
	
	if leases_live:
		effective_now = actual_now
	else:
		effective_now = leases_file_mtime
	
	return (raw_leases, leases_path, leases_file_mtime, oui_db, actual_now, effective_now)


def script_run__report_full(script_path, the_args):
	if len(the_args) > 1:
		print_usage_and_exit(script_path)
	
	db = 'by_ip'
	
	if len(the_args) > 0:
		db = 'by_' + the_args[0].lower()
		
		if db not in ('by_mac', 'by_ip'):
			print_usage_and_exit(script_path)
	
	raw_leases, leases_path, leases_file_mtime, oui_db, actual_now, effective_now = report_common_setup(script_path)
	
	key_list = list(raw_leases[db].keys())
	key_list.sort()
	
	print('+------------------------------------------------------------------------------------------------------------------------------------------')
	print('| DHCPD All Dynamic Leases Report')

	if db == 'by_mac':
		print('+-------------------+-----------------+---------------------+---------------------+---------+----------------------+-----------------------')
		print('| MAC Address       | IP Address      | Began (d,H:M:S)     | Duration (d,H:M:S)  |  State  | Client Manufacturer  | Client Hostname / UID')
		print('+-------------------+-----------------+---------------------+---------------------+---------+----------------------+-----------------------')
	else:
		print('+-----------------+-------------------+---------------------+---------------------+---------+----------------------+-----------------------')
		print('| IP Address      | MAC Address       | Began (d,H:M:S)     | Duration (d,H:M:S)  |  State  | Client Manufacturer  | Client Hostname / UID')
		print('+-----------------+-------------------+---------------------+---------------------+---------+----------------------+-----------------------')

	total_leases = 0
	total_active_leases = 0
	
	for key in key_list:
		for lease in raw_leases[db][key]:
			
			augment_lease_record(lease, effective_now, oui_db)
			
			total_leases = total_leases + 1

			if lease_is_active(lease, effective_now):
				total_active_leases = total_active_leases + 1
			
			outln = '  '
			
			if db == 'by_mac':
				outln = outln + format(lease['hardware'],	'<17') + ' | ' + \
						format(lease['ip_address'],	'<15') + ' | '
			else:
				outln = outln + format(lease['ip_address'],	'<15') + ' | ' + \
						format(lease['hardware'],	'<17') + ' | '
			
			outln = outln + \
				format(str(lease['began']),	'>19') + ' | ' + \
				format(lease['duration'],	'>19') + ' | ' + \
				format(lease['binding'],	'^7')  + ' | ' + \
				format(lease['hardware-company'][:20],	'<20') + ' | ' + \
				lease['client-string']
			
			print(outln)
	
	if db == 'by_mac':
		print('+-------------------+-----------------+---------------------+---------------------+---------+----------------------+-----------------------')
	else:
		print('+-----------------+-------------------+---------------------+---------------------+---------+----------------------+-----------------------')
	
	print('|                    Total Leases =  ' + str(total_leases))
	print('|             Total Active Leases =  ' + str(total_active_leases))
	print('|                     Leases File =  ' + leases_path)
	print('| Leases File Last Modified (UTC) =  ' + str(leases_file_mtime))
	print('|     Report Effective Time (UTC) =  ' + str(effective_now))
	print('|    Report Generation Time (UTC) =  ' + str(actual_now))
	print('+------------------------------------------------------------------------------------------------------------------------------------------')


def script_run__report_active(script_path, the_args):
	if len(the_args) > 0:
		print_usage_and_exit(script_path)
	
	raw_leases, leases_path, leases_file_mtime, oui_db, actual_now, effective_now = report_common_setup(script_path)
	
	active_leases = select_active_leases(raw_leases['by_ip'], effective_now)
	report_dataset = augment_active_leases_list(active_leases, effective_now, oui_db)
	
	print('+--------------------------------------------------------------------------------------------------------------------------------------------------')
	print('| DHCPD Active Dynamic Leases Report')
	print('+-----------------+-------------------+-----------------+---------------------+---------------------+----------------------+-----------------------')
	print('| IP Address      | MAC Address       | MAC Also Has IP | Last Contacted      | Expires In (d,H:M:S)| Client Manufacturer  | Client Hostname / UID')
	print('+-----------------+-------------------+-----------------+---------------------+---------------------+----------------------+-----------------------')
	
	for lease in report_dataset:
		print('  ' + \
			format(lease['ip_address'],		'<15') + ' | ' + \
			format(lease['hardware'],		'<17') + ' | ' + \
			format(lease['also-has-ip'],		'<15') + ' | ' + \
			format(lease['last-contact'],		'>19') + ' | ' + \
			format(lease['expires'],		'>19') + ' | ' + \
			format(lease['hardware-company'][:20],	'<20') + ' | ' + \
			lease['client-string'])
	
	print('+-----------------+-------------------+-----------------+---------------------+---------------------+----------------------+-----------------------')
	print('|             Total Active Leases =  ' + str(len(report_dataset)))
	print('|                     Leases File =  ' + leases_path)
	print('| Leases File Last Modified (UTC) =  ' + str(leases_file_mtime))
	print('|     Report Effective Time (UTC) =  ' + str(effective_now))
	print('|    Report Generation Time (UTC) =  ' + str(actual_now))
	print('+--------------------------------------------------------------------------------------------------------------------------------------------------')


def script_run__filter_by_mac(script_path, the_args):
	if len(the_args) == 0:
		print_usage_and_exit(script_path)
	
	filter_mode = 'out'
	
	if the_args[0].lower() in ('out', 'in'):
		filter_mode = the_args[0].lower()
		the_args = the_args[1:]
	
	if len(the_args) == 0:
		print_usage_and_exit(script_path)
	
	filter_out = (filter_mode == 'out')
	filter_in  = not filter_out
	
	macs_db = {}
	filenames_list = the_args
	
	for filename in filenames_list:
		fh = open(filename, 'r')
		
		for line in fh:
			line = line.partition('#')[0].strip()
			match = search_for_mac(line)
			
			if match:
				mac = get_mac(match)
				macs_db[mac] = 1
			
		fh.close()
	
	n_filtered_lines = 0
	
	for line in sys.stdin:
		match = search_for_mac(line)
		
		if match:
			mac = get_mac(match)
			
			if mac in macs_db:
				if filter_in:
					sys.stdout.write(line)
				else:
					n_filtered_lines = n_filtered_lines + 1
			else:
				if filter_out:
					sys.stdout.write(line)
				else:
					n_filtered_lines = n_filtered_lines + 1
		else:
			sys.stdout.write(line)
	
	sys.stdout.write('| Total Lines Excluded by Filter: ' + str(n_filtered_lines) + '\n')
	sys.stdout.write('+----------------------------------------\n\n')


def script_run__dump_macs(script_path, the_args):
	if len(the_args) > 1:
		print_usage_and_exit(script_path)
	
	mode = 'normal' if len(the_args) == 0 else the_args[0]
	
	if mode not in ('normal', 'shorewall'):
		print_usage_and_exit(script_path)
	
	macs_dict = {}
	
	for line in sys.stdin:
		match = search_for_mac(line)

		if match:
			if mode == 'shorewall':
				mac = '~' + get_mac2(match)
				output_line = mac
				
				columns = [s.strip() for s in line.split('|')]
				
				if len(columns) == 7:
					oui = columns[5]
					client_string = columns[6]
					output_line = output_line + ' # "' + client_string + '" - ' + oui
			
			else:
				mac = get_mac(match)
				output_line = mac
			
			if (mac not in macs_dict) or (len(macs_dict[mac]) < len(output_line)):
				macs_dict[mac] = output_line
	
	macs_sorted = list(macs_dict.keys())
	macs_sorted.sort()
	
	for mac in macs_sorted:
		print(macs_dict[mac])



if __name__ == '__main__':
	script_path = os.path.realpath(__file__)
	
	the_args = sys.argv[1:]
	
	if len(the_args) == 0:
		print_usage_and_exit(script_path)
	
	script_mode = the_args[0].lower()
	
	if script_mode == 'report-active' or script_mode == 'ra':
		script_run = script_run__report_active
	
	elif script_mode == 'report-full' or script_mode == 'rf':
		script_run = script_run__report_full
	
	elif script_mode == 'filter-by-mac' or script_mode == 'fbm':
		script_run = script_run__filter_by_mac
	
	elif script_mode == 'dump-macs' or script_mode == 'dm':
		script_run = script_run__dump_macs
	
	else:
		print_usage_and_exit(script_path)
	
	script_run(script_path, the_args[1:])

